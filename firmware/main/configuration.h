#ifndef APP_TEMPLATE_CONFIGURATION_H
#define APP_TEMPLATE_CONFIGURATION_H

#include <limits>

namespace sound_configuration {
    constexpr int SAMPLE_RATE = 31000;
    constexpr int ACTIVE_LOOPS = 15;
    constexpr int16_t SAMPLE_MAX = std::numeric_limits<int16_t>::max();
}

#endif //APP_TEMPLATE_CONFIGURATION_H
