#ifndef MAIN_MODESETTINGS_H_
#define MAIN_MODESETTINGS_H_

#include "Mode.h"
#include <CPPNVS.h>

#define BUTTON_CONFIRM 2
#define BUTTON_UP 3
#define BUTTON_DOWN 1


class ModeSettings: public Mode {
public:
	WaveLoop* generateNewWl();

	ModeSettings(std::string accessString, uint32_t min_value, uint32_t max_value, uint32_t step_size);
	virtual void apply(Modulator* mod, uint16_t current_buttons, uint16_t previous_buttons, uint64_t current_time, uint64_t button_press_time) override;

private:
    static const char nvs_namespace[];
    NVS nvs;
	uint32_t currentValue;
	std::string accessString;
	uint32_t min_value;
    uint32_t max_value;
    uint32_t step_size;

};

#endif /* MAIN_MODESETTINGS_H_ */
