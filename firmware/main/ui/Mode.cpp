#include "Mode.h"

const chord CHORD_SINGLE = {
	.chord = {0,-1,-1,-1,-1}
};

const chord CHORD_OCTAVE = {
	.chord = {0,12,-1,-1,-1}
};

const chord CHORD_11 = {
		.chord = {0,11,-1,-1,-1}
};

const chord CHORD_SOOF = {
		.chord = {0,4,10,-1,-1}
};

const chord CHORD_NONE_GUITAR = {
		.chord = {-1,8,0,4,10}
};

const chord CHORD_MAJOR_PIANO = {
		.chord = {0,4,7,-1,-1}
};

const chord CHORD_MINOR_PIANO = {
		.chord = {0,3,7,-1,-1}
};

chord Mode::get_chord_by_buttons_play(uint16_t buttons){
	switch(buttons >> 5){
	case 0:
		return CHORD_SINGLE;
	case 2:
		return CHORD_MAJOR_PIANO;
	case 4:
		return CHORD_MINOR_PIANO;
	case 6:
		return CHORD_OCTAVE;
	case 8:
		return CHORD_11;
	default:
		return CHORD_SINGLE;
	}
	//TODO choose reasonable chords
}


Mode::Mode() {
	// TODO Auto-generated constructor stub

}

Mode::~Mode() {
	// TODO Auto-generated destructor stub
}

