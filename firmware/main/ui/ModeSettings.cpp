#include "ModeSettings.h"
#include "WaveLoop.h"
#include "../Controller.h"
#include "sound/data/hull_shape.h"
#include "sound/data/frequencies.h"
#include <cstdio>
#include "configuration.h"

const char ModeSettings::nvs_namespace[] = "controller";

ModeSettings::ModeSettings(std::string accessString, uint32_t min_value, uint32_t max_value, uint32_t step_size):
    nvs(nvs_namespace, NVS_READWRITE)
{
	this->accessString = accessString;
	this->max_value = max_value;
	this->min_value = min_value;
	this->step_size = step_size;
	nvs.get(accessString, this->currentValue);
}

WaveLoop* ModeSettings::generateNewWl(){
	uint32_t octave;
	nvs.get("octave", octave);
	uint32_t volume;
	nvs.get("volume", volume);
	uint32_t hull_shape_index;
	nvs.get("hull_shape", hull_shape_index);
	hull_shape_t* hull_shape = hull_shape_array[hull_shape_index];
	int16_t amplitude = 0.01f * volume * sound_configuration::SAMPLE_MAX / sound_configuration::ACTIVE_LOOPS;
	return new WaveLoop(scale_frequencies[0]* static_cast<float>(1u<<octave), amplitude, hull_shape);
}


void ModeSettings::apply(Modulator* mod, uint16_t current_buttons, uint16_t previous_buttons, uint64_t current_time, uint64_t button_press_time){
	static WaveLoop* replacement_wl = mod->getWaveLoop(0);;
	if(current_buttons != previous_buttons){
		if(current_buttons & (1 << BUTTON_UP)){
			this->currentValue += this->step_size;
			if(this->currentValue > this->max_value){
				this->currentValue = this->max_value;
			}
		}
		if(current_buttons & (1 << BUTTON_DOWN)){
			this->currentValue -= this->step_size;
			if(this->currentValue < this->min_value){
				this->currentValue = this->min_value;
			}
		}
		nvs.set(this->accessString, this->currentValue);
		if(current_buttons & (1 << BUTTON_CONFIRM)){
			nvs.set(this->accessString, this->currentValue);
		}
		printf("%s: %d\n", this->accessString.c_str(), this->currentValue);
		fflush(stdout);
		replacement_wl = generateNewWl();
		mod->setTone(replacement_wl, 0);
		mod->pressTone(0);
	}
}
