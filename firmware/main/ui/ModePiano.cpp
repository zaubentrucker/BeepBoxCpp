#include "ModePiano.h"
#include <stdint.h>


static uint16_t get_key_index_by_buttons(uint16_t buttons){
	uint16_t index = buttons & 0b0000000000001111;


	if(index > 6){
		index--;
	}

	if((buttons >> 4) & 1){ // shift one octave with button 5
		index+=13;
	}

	if(index > 13){
		index--;
	}
	return index-2;
}



ModePiano::ModePiano() {
	// TODO Auto-generated constructor stub

}

ModePiano::~ModePiano() {
	// TODO Auto-generated destructor stub
}

void ModePiano::apply(Modulator* mod, uint16_t current_buttons, uint16_t previous_buttons, uint64_t current_time, uint64_t button_press_time){

	static uint16_t prev_tone_index = get_key_index_by_buttons(previous_buttons);
	static chord_t prev_chord = get_chord_by_buttons_play(previous_buttons);
	static bool press_registered = false;

	if(previous_buttons != current_buttons){
		press_registered = false;
	}


	bool button_held_enough = current_time - button_press_time > BUTTON_HOLD_THRESHOLD;
	bool tone_change_required = button_held_enough && !press_registered;


	if(tone_change_required){
		press_registered = true;
		if(prev_tone_index < 1000){
			for(int i = 0; i<MAX_KEYS_PER_CHORD; i++){
				if(prev_chord.chord[i] >= 0){
					mod->releaseTone(((uint8_t) prev_chord.chord[i]) + prev_tone_index);
				}
				else{
					break;
				}
			}
		}
		uint16_t tone_index = get_key_index_by_buttons(current_buttons);
		chord_t chord = get_chord_by_buttons_play(current_buttons);
		prev_tone_index = tone_index;
		prev_chord = chord;
		if(tone_index < 1000){
			for(int i = 0; i<MAX_KEYS_PER_CHORD; i++){
				if(chord.chord[i] >= 0){
					mod->pressTone(((uint8_t) chord.chord[i]) + tone_index);
				}
				else{
					break;
				}
			}
		}
	}
}

