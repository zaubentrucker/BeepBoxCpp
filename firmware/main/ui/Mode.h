#ifndef MAIN_MODE_H_
#define MAIN_MODE_H_

#include "stdint.h"
#include "Modulator.h"

#define BUTTON_HOLD_THRESHOLD (40000ULL)
#define MAX_KEYS_PER_CHORD 5

typedef struct chord{
	int8_t chord [MAX_KEYS_PER_CHORD];
} chord_t;



class Mode {
public:
	Mode();
	virtual ~Mode();
	virtual void apply(Modulator* mod, uint16_t current_buttons, uint16_t previous_buttons, uint64_t current_time, uint64_t button_press_time) = 0;

protected:
	chord get_chord_by_buttons_play(uint16_t buttons);
};

#endif /* MAIN_MODE_H_ */
