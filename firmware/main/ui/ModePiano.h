#ifndef MAIN_MODEPIANO_H_
#define MAIN_MODEPIANO_H_

#include "Mode.h"

class ModePiano: public Mode {
public:
	ModePiano();
	virtual ~ModePiano();
	virtual void apply(Modulator* mod, uint16_t current_buttons, uint16_t previous_buttons, uint64_t current_time, uint64_t button_press_time) override;
};

#endif /* MAIN_MODEPIANO_H_ */
