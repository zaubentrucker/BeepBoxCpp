#include "test_apps.h"
#include "peripherals/Buttons.h"
#include <iostream>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

[[noreturn]] void test_buttons(){
    Buttons b(true);
    while (true){
        std::cout << b.getState() << std::endl;
        vTaskDelay(10);
    }
}