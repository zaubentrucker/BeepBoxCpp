#include <driver/gpio.h>
#include <cassert>
#include "Buttons.h"
#include <GPIO.h>
#include <memory>
#include <list>
#include <vector>
#include "util/HasCallback.h"

/** the pins that are used for the buttons in the order they are evaluated as bits in a result vector */
static const std::vector<gpio_num_t> buttons =  {
        GPIO_NUM_4,
        GPIO_NUM_35,
        GPIO_NUM_27,
        GPIO_NUM_26,
        GPIO_NUM_34,
        GPIO_NUM_18,
        GPIO_NUM_23,
        GPIO_NUM_21,
        GPIO_NUM_19,
        GPIO_NUM_22
};

static void dummy_isr(void* arg){}

static void buttons_isr(void* arg){
    Buttons* calling_instance = static_cast<Buttons*>(arg);
    calling_instance->updateState();
    calling_instance->runCallback();
}

Buttons::Buttons(bool updateFromInterrupt):
    callbackObj_(nullptr)
{

    // configure gpio
    // -----------------
    for(auto button : buttons) {
        ESP32CPP::GPIO::setInput(button);
        gpio_set_pull_mode(button, GPIO_PULLUP_ONLY);
        if(updateFromInterrupt) {
            ESP32CPP::GPIO::setInterruptType(button, GPIO_INTR_ANYEDGE);
            ESP32CPP::GPIO::addISRHandler(button, buttons_isr, (void *) this);
        }
    }

    button_state_ = 0;
    updateState();
}

uint16_t Buttons::readButtons(){
    uint64_t raw_buttons = 0;
    raw_buttons = ((uint64_t)gpio_input_get_high()) << 32u;
    raw_buttons |= gpio_input_get();
    uint16_t res = 0;
    for(unsigned int i = 0; i < BUTTON_COUNT; i++){
        uint64_t button_mask = ((uint64_t)1) << ((uint64_t) buttons[i]);
        bool button_set = (raw_buttons & button_mask) == 0;
        if(button_set){
            res |= 1u << i;
        }
    }
    return res;
}

uint16_t Buttons::getState() const {
    return button_state_;
}

void Buttons::updateState(){
    button_state_ = readButtons();
}

Buttons::~Buttons() {
    for(auto button: buttons){
        ESP32CPP::GPIO::interruptDisable(button);
        ESP32CPP::GPIO::addISRHandler(button, dummy_isr, nullptr);
    }
}

void Buttons::attachCallback(HasCallback *callbackObj) {
    callbackObj_ = callbackObj;
}

void Buttons::runCallback() {
    if(this->callbackObj_) {
        this->callbackObj_->callBack();
    }
}
