//
// Created by kokke on 19.03.20.
//

#ifndef APP_TEMPLATE_INTERICSOUND_H
#define APP_TEMPLATE_INTERICSOUND_H


#include <vector>
#include <cstdint>


#define I2S_NUM     I2S_NUM_0


class InterICSound {
public:
    InterICSound(int sample_rate);
    void writeDMA(std::vector<int16_t> &data);
};


#endif //APP_TEMPLATE_INTERICSOUND_H
