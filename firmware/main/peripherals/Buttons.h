#ifndef APP_TEMPLATE_BUTTONS_H
#define APP_TEMPLATE_BUTTONS_H

#define BUTTON_COUNT 10

#define BUTTON_VALUE_UP 3
#define BUTTON_VALUE_DOWN 1
#define VOLUME_STEP 10
#define VOLUME_MAX 100
#define VOLUME_MIN 0
#define KEY_MAX 3
#define HULL_MAX 3
#define HULL_MIN 0

/*
 * This class is used to read the button state. When
 */

#include <cstdint>
#include <stdexcept>
#include "util/HasCallback.h"

class Buttons {
public:

    /**
     * Returns the state of the buttons since the last update.
     * @return state of the buttons as bits
     */
    uint16_t getState() const;

    /**
     * Updates the state of the buttons by reading from gpio.
     */
    void updateState();

    /**
     * CAUTION!!!: destruction of the button will disable interrupts for the button pins and replace the isr with a
     * dummy!
     * @param updateFromInterrupt determines whether gpio interrupts will automatically refresh the button_state_
     */
    Buttons(bool updateFromInterrupt);
    ~Buttons();

    void attachCallback(HasCallback* callbackObj);
    void runCallback();

private:
    static uint16_t readButtons();
    uint16_t button_state_;
    HasCallback* callbackObj_;
};


#endif //APP_TEMPLATE_BUTTONS_H
