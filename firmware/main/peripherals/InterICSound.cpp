//
// Created by kokke on 19.03.20.
//

#include "InterICSound.h"
#include "freertos/FreeRTOS.h"
#include "driver/i2s.h"
#include "driver/gpio.h"

InterICSound::InterICSound(int sample_rate) {
    i2s_config_t i2s_config = {
            .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX),
            .sample_rate = sample_rate,
            .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,
            .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
            .communication_format = (i2s_comm_format_t)(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB),
            .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1, // high interrupt priority
            .dma_buf_count = 8,
            .dma_buf_len = 32,
            .use_apll = 0,
            .tx_desc_auto_clear = 0,
            .fixed_mclk = 0 // not used since .use_apll is false
    };
    i2s_pin_config_t pin_config = {
            .bck_io_num = 32,
            .ws_io_num = 25,
            .data_out_num = 33,
            .data_in_num = -1,    //Not used
    };


    i2s_driver_install(I2S_NUM, &i2s_config, 0, nullptr);
    i2s_set_pin(I2S_NUM, &pin_config);
}

void InterICSound::writeDMA(std::vector<int16_t> &data) {
    int16_t* data_array = data.data();
    size_t bytes_written_total = 0;
    size_t bytes_written_last = 0;
    while(bytes_written_total < data.size()){
        i2s_write(I2S_NUM, &(data_array[bytes_written_total]),
                data.size() * sizeof(int16_t) - bytes_written_total, &bytes_written_last, 100);
        bytes_written_total += bytes_written_last;
    }

}