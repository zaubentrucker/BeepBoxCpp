#include "Controller.h"
#include "Modulator.h"
#include "WaveLoop.h"
#include <cstdio>
#include "ModePiano.h"
#include "ModeSettings.h"
#include "esp_timer.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "Buttons.h"


Controller::Controller(Modulator *mod) :
        buttons_(true),
        modulator_(mod)
{
    this->currentTime = 0;
	this->updateCurrentTime();
    this->buttonPressTime = 0;
	this->setButtonsChanged();
    this->currentButtons = buttons_.getState();
	this->previousButtons = this->currentButtons;
	switch(this->currentButtons){
	case 0:
		this->current_mode = new ModePiano();
		break;
	case 1:
		this->current_mode = new ModeSettings("volume", 0, 100, 10);
		break;
	case 2:
		this->current_mode = new ModeSettings("octave", 0, 3, 1);
		break;
	case 3:
		this->current_mode = new ModeSettings("hull_shape", 0, 1, 1);
		break;
	default:
		this->current_mode = new ModePiano();
	}
}

Controller::~Controller() {
	delete this->current_mode;
}

uint64_t Controller::getCurrentTime(){
	return this->currentTime;
}

uint64_t Controller::getButtonPressTime(){
	return this->buttonPressTime;
}

void Controller::setButtonsChanged(){
	this->buttonPressTime = this->currentTime;
}

void Controller::updateCurrentTime(){
	this->currentTime = esp_timer_get_time();
}

void Controller::update(){
	this->updateCurrentTime();
	this->previousButtons = this->currentButtons;
	this->currentButtons = buttons_.getState();
	if(this->previousButtons != this->currentButtons){
		this->setButtonsChanged();
	}
	this->current_mode->apply(this->modulator_, this->currentButtons, this->previousButtons, this->currentTime, this->buttonPressTime);

}