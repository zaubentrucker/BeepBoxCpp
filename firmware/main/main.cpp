#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "configuration.h"
#include <esp_heap_caps.h>
#include <cstdio>
#include "Buttons.h"
#include "Controller.h"
#include "peripherals/InterICSound.h"
#include "Modulator.h"
#include "WaveLoop.h"
#include "sound/data/hull_shape.h"
#include "sound/data/frequencies.h"
#include <iostream>
#include <CPPNVS.h>
#include "test_apps.h"

const size_t BUFFER_SIZE = 200;

void start(int sample_rate);

bool restart_scheduled_ = false;

TaskHandle_t backend_handle = nullptr;
TaskHandle_t ui_handle = nullptr;

Modulator* mod;
Controller* contr;

static bool app_backend_task_terminated = false;
void app_backend_task(void* pvParameters){
    InterICSound i2c = InterICSound(sound_configuration::SAMPLE_RATE);

    std::vector<int16_t > sample_buffer (BUFFER_SIZE, 0);
    while (!restart_scheduled_) {

        for(int16_t & i : sample_buffer){
            i = 0;
        }

        mod->produceSamples(sample_buffer);
        i2c.writeDMA(sample_buffer);
        //vTaskDelay(1);
    }

    app_backend_task_terminated = true;
}

static bool app_ui_task_terminated = false;
void app_ui_task(void* pvParameters){
    contr = new Controller(mod);
    while (!restart_scheduled_){
        contr->update();
        taskYIELD();
    }

    app_ui_task_terminated = true;
}

extern "C" void app_main(){

    WaveLoop::init(scale_frequencies[0], sound_configuration::SAMPLE_RATE);

    NVS nvs("controller", NVS_READONLY);
    uint32_t octave;
    nvs.get("octave", octave);
    uint32_t volume;
    nvs.get("volume", volume);
    uint32_t hull_shape_index;
    nvs.get("hull_shape", hull_shape_index);

    octave = 2;
    volume = 100;
    hull_shape_index = 0;

    hull_shape_t* hull_shape = hull_shape_array[hull_shape_index];

    auto sample_max = (sound_configuration::SAMPLE_MAX/sound_configuration::ACTIVE_LOOPS);
    sample_max *= static_cast<uint16_t>(volume / 100.0);

    printf("RAM left %d\n", heap_caps_get_free_size(MALLOC_CAP_8BIT));

    mod = new Modulator(FREQUENCIES_COUNT*FREQUENCIES_LENGTH, sound_configuration::ACTIVE_LOOPS);

    for(unsigned int i = 0; i<FREQUENCIES_COUNT; i++){
        unsigned int factor = (1u << i) * (1u << octave);
        for(float scale_frequency : scale_frequencies){
            auto* wl = new WaveLoop(scale_frequency * static_cast<float>(factor), sample_max, hull_shape);
            mod->addTone(wl);
        }
    }

    printf("RAM left %d\n", heap_caps_get_free_size(MALLOC_CAP_8BIT));


    xTaskCreatePinnedToCore(app_backend_task, "backend", 2*4096, nullptr, 6, &backend_handle, 0);
    xTaskCreatePinnedToCore(app_ui_task, "ui", 2*4096, nullptr, 5, &ui_handle, 1);
}