#include "Modulator.h"
#include <cstdlib>
#include <cstdio>
#include "DummyWaveLoop.h"

WaveLoop* dummyWaveLoop = new DummyWaveLoop();

Modulator::Modulator(
		uint16_t waveLoop_count,
		uint8_t active_waveLoop_max) {

	this->waveLoops = new WaveLoop*[waveLoop_count];
	for(int i=0; i<waveLoop_count; i++){
		this->waveLoops[i] = dummyWaveLoop;
	}

	this->waveLoopCount = waveLoop_count;
	this->activeLoopMax = active_waveLoop_max;
}

Modulator::~Modulator() {

	for(int i = 0; i<this->waveLoopCount; i++){
		if(this->waveLoops[i] != dummyWaveLoop){
			delete this->waveLoops[i];
		}
	}
	delete this->waveLoops;
}

bool Modulator::isDummy(WaveLoop* w){
	return w == dummyWaveLoop;
}


void Modulator::setWaveLoop(WaveLoop* w, uint16_t index){
	if(index >= this->waveLoopCount){
		printf("setWaveLoop1\n");
		fflush(stdout);
	}
	this->waveLoops[index] = w;
}

WaveLoop* Modulator::getWaveLoop(uint16_t index){
	if(index >= this->waveLoopCount){
		printf("getWaveLoop1\n");
		fflush(stdout);
		while(1);
	}
	else{
		return this->waveLoops[index];
	}
}


void Modulator::addTone(WaveLoop* w){
	for(int i = 0; i<this->waveLoopCount; i++){
		if(isDummy(this->waveLoops[i])){
			this->setWaveLoop(w, i);
			break;
		}
	}
}

void Modulator::setTone(WaveLoop* w, uint16_t index){
	this->setWaveLoop(w, index);
}

void Modulator::muteSilent(){
	int checked_actives = 0;
	int ptr = 0;
	float minimum = 1000; //magic number! hopefully nobody will choose volumes as high as 1000
	WaveLoop* minimal_w = nullptr;
	while(ptr < this->waveLoopCount){
		WaveLoop* current = this->waveLoops[ptr];
		ptr++;
		if(current->getState() != WaveLoop_INACTIVE && current->getCurrentVolume() < minimum){
			minimal_w = current;
			minimum = current->getCurrentVolume();
			checked_actives++;
		}
	}
	minimal_w->setState(WaveLoop_INACTIVE);
}

int16_t Modulator::getActiveLoopCount(){
	int16_t result = 0;
	for(int i = 0; i < this->waveLoopCount; i++){
		if(this->waveLoops[i]->getState() != WaveLoop_INACTIVE){
			result++;
		}
	}
	return result;
}

void Modulator::pressTone(uint16_t index){
	WaveLoop* wl = this->getWaveLoop(index);

	if(wl->getState() == WaveLoop_INACTIVE && this->getActiveLoopCount() > this->activeLoopMax){
		this->muteSilent();
	}
	wl->setState(WaveLoop_ATTACK);
}

void Modulator::releaseTone(uint16_t index){
	WaveLoop* wl = this->getWaveLoop(index);
	enum WaveLoopState state = wl->getState();
	if(state == WaveLoop_ATTACK || state == WaveLoop_DECAY || state == WaveLoop_SUSTAIN){
		wl->setState(WaveLoop_RELEASE);
	}
}

void Modulator::muteTone(uint16_t index){
	this->getWaveLoop(index)->setState(WaveLoop_INACTIVE);
}

void Modulator::removeTone(uint16_t index){
	this->setWaveLoop(dummyWaveLoop, index);
}

void Modulator::produceSamples(std::vector<int16_t> &target_buffer){
	for(int i = 0; i<target_buffer.size()/2; i++){
		int16_t sample = 0;
		for(int j = 0; j<this->waveLoopCount; j++){
			this->waveLoops[j]->addToSample(&sample);
		}
        target_buffer[i * 2] += sample;
        target_buffer[i * 2 + 1] += sample;
	}
}
