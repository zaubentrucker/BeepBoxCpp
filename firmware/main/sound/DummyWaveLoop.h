#ifndef MAIN_DUMMYWAVELOOP_H_
#define MAIN_DUMMYWAVELOOP_H_

#include "WaveLoop.h"

class DummyWaveLoop: public WaveLoop {
public:
	DummyWaveLoop();
	virtual ~DummyWaveLoop();
	virtual void addToSample(int16_t* sample) override;
	enum WaveLoopState getState() override;
};

#endif /* MAIN_DUMMYWAVELOOP_H_ */
