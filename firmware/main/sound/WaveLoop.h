#ifndef MAIN_WAVELOOP_H_
#define MAIN_WAVELOOP_H_

#include <stdint.h>
#include "sound/data/hull_shape.h"

#define WAVE_BUFFER_MULTIPLICITY 1

enum WaveLoopState{
	WaveLoop_ATTACK,
	WaveLoop_DECAY,
	WaveLoop_SUSTAIN,
	WaveLoop_RELEASE,
	WaveLoop_INACTIVE
};


class WaveLoop {

public:

	static void init(float min_frequency, int16_t sample_rate);
	static int sample_rate;
	static float min_frequency;


	WaveLoop(float frequency, int16_t max_volume, hull_shape_t* hull_shape);
	WaveLoop();
	virtual ~WaveLoop();
	virtual void addToSample(int16_t* sample);
	virtual enum WaveLoopState getState();
	virtual void setState(enum WaveLoopState);
	virtual float getCurrentVolume();
	bool operator==(WaveLoop& other);

protected:
	hull_shape_t* hull_shape_;
	int16_t* waveData_;
	uint32_t waveDataPtr_;
	uint32_t waveDataLength_;
	float volume_;
	int16_t max_volume_;
	enum WaveLoopState state_;


	void fillWaveData(float frequency);
};

#endif /* MAIN_WAVELOOP_H_ */
