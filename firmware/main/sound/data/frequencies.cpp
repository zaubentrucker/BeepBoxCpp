#include "frequencies.h"

const float scale_frequencies[FREQUENCIES_LENGTH] = {
		32.7032f, //C
		34.6478f, //C#
		36.7081f, //D
		38.8909f, //D#
		41.2034f, //E
		43.6535f, //F
		46.2493f, //F#
		48.9994f, //G
		51.9131f, //G#
		55.0000f, //A
		58.2705f, //A#
		61.7354f,  //B
};
