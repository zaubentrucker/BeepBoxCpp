/*
 * hull_shape.c
 *
 *  Created on: Sep 13, 2018
 *      Author: kokke
 */


#include "hull_shape.h"



hull_shape_t hull_shape_drum = {
	.ATTACK_STEP = (0.0083f),
	.DECAY_STEP  = (0.005f),
	.SUSTAIN_STEP = (0.00015f),
	.RELEASE_STEP = (0.00015f),
	.ATTACK_MAX = 1,
	.DECAY_MIN = 0.2f
};


hull_shape_t hull_shape_piano = {
	.ATTACK_STEP = (0.002f),
	.DECAY_STEP  = (0.003f),
	.SUSTAIN_STEP = (0.000003f),
	.RELEASE_STEP = (0.00005f),
	.ATTACK_MAX = 1,
	.DECAY_MIN = 0.90f
};


hull_shape_t* hull_shape_array[] = {
		&hull_shape_piano,
		&hull_shape_drum
};
