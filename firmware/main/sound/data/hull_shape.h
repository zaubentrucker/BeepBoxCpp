/*
 * hull_shape.h
 *
 *  Created on: Sep 13, 2018
 *      Author: kokke
 */

#ifndef MAIN_HULL_SHAPE_H_
#define MAIN_HULL_SHAPE_H_



typedef const struct hull_shape {
	float ATTACK_STEP;
	float DECAY_STEP;
	float SUSTAIN_STEP;
	float RELEASE_STEP;

	float ATTACK_MAX;
	float DECAY_MIN;
} hull_shape_t;

extern hull_shape_t hull_shape_drum;
extern hull_shape_t hull_shape_piano;
extern hull_shape_t* hull_shape_array[];


#endif /* MAIN_HULL_SHAPE_H_ */
