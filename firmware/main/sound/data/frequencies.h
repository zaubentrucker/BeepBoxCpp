/*
 * frequencies.h
 *
 *  Created on: Sep 13, 2018
 *      Author: kokke
 */

#ifndef MAIN_FREQUENCIES_H_
#define MAIN_FREQUENCIES_H_


#define FREQUENCIES_LENGTH 12
#define FREQUENCIES_COUNT 3

extern const float scale_frequencies[FREQUENCIES_LENGTH];


#endif /* MAIN_FREQUENCIES_H_ */
