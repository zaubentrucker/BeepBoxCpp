#ifndef MAIN_MODULATOR_H_
#define MAIN_MODULATOR_H_

#include <stdint.h>
#include <vector>
#include "WaveLoop.h"


class Modulator {
public:
	Modulator(uint16_t waveLoop_count, uint8_t active_queue_count);
	virtual ~Modulator();

	/* accumulates samples as the sum of the set wave loops into target_buff
	 * @param[OUT] target_buff: buffer to write to CONTENTS ARE ADDED, NOT INITIALIZED ARRAYS PRODUCE ERRORS
	 * @param buffsize: maximum of bytes that can be written to target_buff HAS TO BE AN EVEN NUMBER
	 */
	virtual void produceSamples(std::vector<int16_t> &target_buffer);

	/* functions modify the current set of played tones
	 */
	virtual void pressTone(uint16_t index);
	virtual void releaseTone(uint16_t index);
	virtual void muteTone(uint16_t index);

	/* functions that add or remove WaveLoops to/from the modulator
	 */
	virtual void addTone(WaveLoop* w);
	virtual void setTone(WaveLoop* w, uint16_t index);
	virtual void removeTone(uint16_t index);
	virtual WaveLoop* getWaveLoop(uint16_t index);

protected:
	WaveLoop** waveLoops;
	uint16_t waveLoopCount;
	uint16_t activeLoopMax;

	virtual void setWaveLoop(WaveLoop* w, uint16_t index);
	void decrementActiveLoopCount();
	void incrementActiveLoopCount();
	int16_t getActiveLoopCount();

	/* frees the WaveLoop slot of the quietest active WaveLoop
	 */
	virtual void muteSilent();

	static bool isDummy(WaveLoop* w);
};

#endif /* MAIN_MODULATOR_H_ */
