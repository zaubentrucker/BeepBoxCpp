#include "WaveLoop.h"
#include <cmath>

#define PI 3.14159265f

float WaveLoop::min_frequency = 0;
int WaveLoop::sample_rate = 0;


void WaveLoop::init(float min_freq, int16_t sample_r){
	sample_rate = sample_r;
	min_frequency = min_freq;
}

WaveLoop::WaveLoop(){
	this->state_ = WaveLoop_INACTIVE;
	this->waveDataLength_ = 0;
	this->volume_ = 0;
	this->max_volume_ = 0;
	this->waveDataPtr_ = 0;

	this->waveData_ = new int16_t[0];
	this->hull_shape_ = nullptr;
}

WaveLoop::WaveLoop(float frequency, int16_t max_volume, hull_shape_t* hull_shape){
	this->waveDataLength_ = (int) (round((WAVE_BUFFER_MULTIPLICITY * sample_rate) / frequency));
	this->waveData_ = new int16_t[this->waveDataLength_];
	this->max_volume_ = max_volume;
	this->waveDataPtr_ = 0;
	this->state_ = WaveLoop_INACTIVE;
	this->volume_ = 0;
	this->hull_shape_ = hull_shape;

	this->fillWaveData(frequency);
}

WaveLoop::~WaveLoop() {
	delete this->waveData_;
}


void WaveLoop::fillWaveData(float frequency){
	for(int i = 0; i < this->waveDataLength_; i++){
		float current_phase = i * ((2 * PI * WAVE_BUFFER_MULTIPLICITY) / this->waveDataLength_);
		float value = round((sin(current_phase) * ((float) this->max_volume_)) / (frequency / min_frequency));
		this->waveData_[i] = (int16_t) value;
	}
}

bool WaveLoop::operator==(WaveLoop& other)
{
    return this == &other;
}


void WaveLoop::addToSample(int16_t* sample){
	if(this->state_ != WaveLoop_INACTIVE){
		*sample += (int16_t) this->waveData_[this->waveDataPtr_] * this->volume_;
		this->waveDataPtr_ = (this->waveDataPtr_ + 1) % this->waveDataLength_;
		if(this->state_ != WaveLoop_ATTACK){
		}
		switch(this->state_){
		case WaveLoop_ATTACK:
			if(this->volume_ + this->hull_shape_->ATTACK_STEP >= 1){
				this->state_ = WaveLoop_DECAY;
			}
			else{
				this->volume_ += this->hull_shape_->ATTACK_STEP;
			}
			break;
		case WaveLoop_DECAY:
			if(this->volume_ - this->hull_shape_->DECAY_STEP <= this->hull_shape_->DECAY_MIN){
				this->state_ = WaveLoop_SUSTAIN;
			}
			else{
				this->volume_ -= this->hull_shape_->DECAY_STEP;
			}
			break;
		case WaveLoop_SUSTAIN:
			if(this->volume_ - this->hull_shape_->SUSTAIN_STEP >= 0){
				this->volume_ -= this->hull_shape_->SUSTAIN_STEP;
			}
			else{
				this->volume_ = 0;
				this->state_ = WaveLoop_INACTIVE;
			}
			break;
		case WaveLoop_RELEASE:
			if(this->volume_ - this->hull_shape_->RELEASE_STEP >= 0){
				this->volume_ -= this->hull_shape_->RELEASE_STEP;
			}
			else{
				this->volume_ = 0;
				this->state_ = WaveLoop_INACTIVE;
			}
			break;
		case WaveLoop_INACTIVE:
			break;
		}
	}
}

void WaveLoop::setState(enum WaveLoopState state){
	this->state_ = state;
	if(state == WaveLoop_INACTIVE){
		this->volume_ = 0;
	}
}


enum WaveLoopState WaveLoop::getState(){
	return this->state_;
}

float WaveLoop::getCurrentVolume(){
	return this->volume_;
}
