#include "DummyWaveLoop.h"
#include "WaveLoop.h"

hull_shape_t dummy_hull_shape = {0,0,0,0,0,0};

DummyWaveLoop::DummyWaveLoop(){
	this->state_ = WaveLoop_INACTIVE;
}

DummyWaveLoop::~DummyWaveLoop() {
}

void DummyWaveLoop::addToSample(int16_t* sample){
	return;
}

enum WaveLoopState DummyWaveLoop::getState(){
	return WaveLoop_INACTIVE;
}
