#ifndef MAIN_CONTROLLER_H_
#define MAIN_CONTROLLER_H_

#include "Modulator.h"
#include "Mode.h"
#include "peripherals/Buttons.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define BUTTON_COUNT 10

#define BUTTON_VALUE_UP 3
#define BUTTON_VALUE_DOWN 1
#define VOLUME_STEP 10
#define VOLUME_MAX 100
#define VOLUME_MIN 0
#define KEY_MAX 3
#define HULL_MAX 3
#define HULL_MIN 0

class Controller{
public:
	explicit Controller(Modulator *mod);
	virtual ~Controller();
	void update();

private:
	Buttons buttons_;
	Modulator* modulator_;

protected:
	uint64_t currentTime;
	uint64_t buttonPressTime;
	uint16_t currentButtons;
	uint16_t previousButtons;
	Mode* current_mode;

	uint64_t getCurrentTime();
	void updateCurrentTime();
	uint64_t getButtonPressTime();
	void setButtonsChanged();
};

#endif /* MAIN_CONTROLLER_H_ */
