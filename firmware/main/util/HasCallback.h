//
// Created by kokke on 20.04.20.
//

#ifndef APP_TEMPLATE_HASCALLBACK_H
#define APP_TEMPLATE_HASCALLBACK_H


class HasCallback {
public:
    virtual void callBack();
};


#endif //APP_TEMPLATE_HASCALLBACK_H
