# BeepBoxCpp

The BeepBoxCpp is the third iteration of my microcontroller-based instruments.
It currently features:
* 2 octaves of sine tones incl. half tones
* A set of chords
* Volume Control
* Selectable base octave

## Second Version
![second_version](https://git.rwth-aachen.de/zaubentrucker/BeepBoxCpp/-/raw/master/version_02.jpeg)

## First Version
![first version](https://git.rwth-aachen.de/kokke/BeepBoxCpp/raw/master/version_01.jpg)

The hardware consists mainly of an esp32-wroom and an external dac. If you go shopping at the store with the big A and E, you can build this for approx. 15EUR + 1.5EUR costs for filament for 3d printing (or however you manufacture the case might cost).

If anybody wants to make this, just write me an email and I might create a tutorial here.
