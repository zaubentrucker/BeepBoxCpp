include <non_printed.scad>
include <MCAD/nuts_and_bolts.scad>

//helper variables
//-----------------------
clip_avoid = 0.01;
debug = true;
//-----------------------


//global values that I define
//-----------------------
$fn=20;
bad_printer_offset = 0.25;
bad_printer_factor = 1.01;

alignment_pin_height = 4.5;
alignment_pin_length = 6;
alignment_pin_thickness = 2;
bottom_champfer_radius = 3;
top_champfer_radius = 3;
metric_screw_size = 3;
audio_jack_length = 1.5;
audio_jack_diameter = 5;
audio_jack_offset = 23.75;
audio_plug_diameter = 10;
wall_thickness = 4;
case_height = 40;
case_slot_width = 4;
pcb_top_layer_spacing = 15;
pcb_bottom_layer_spacing = 6.5; //space reserved for the pins/components on the bottom layer
recess_width = 2;
additional_border_height = 1;
usb_cable_height = 7.6;
usb_cable_width = 11;
button_plate_front_z_offset = -2; //move button plate  front down to make room for the usb plug
power_button_width = 8.75;
power_button_length = 13.13;
power_button_offset = 25.5;
//-----------------------


//dependent global values
//-----------------------
mainboard_x_offset = -nodeMCU_x_offset - wall_thickness + clad_thickness - clip_avoid; //arrises from the offset of the usb connector
podest_x_clearance = audio_jack_length;
podest_y_clearance = mainboard_champfer_radius;
case_width = button_plate_front_length + wall_thickness*2;
case_top_height = wall_thickness+7; //TODO figure this value out
case_bottom_base_thickness = wall_thickness;
case_bottom_wall_height = button_plate_front_width + wall_thickness;
case_bottom_height = case_bottom_base_thickness + case_bottom_wall_height;
case_length = mainboard_x_offset + mainboard_length + wall_thickness*2 + bad_printer_offset*2;
spacer_thickness = metric_screw_size*2.8;
button_plate_border_offset = top_champfer_radius;
button_plate_x_offset = top_champfer_radius;
bottom_compontent_holder_height = max(bat_board_max_height, battery_height);
//-----------------------

//translate([0,0,case_bottom_height + 10]) 
case_top();
//case_bottom();

/** creates a quarter of a cylinder with given height and radius
*/
module quarter_cylinder(r=false, h=false){
  intersection(){
    cylinder(r=r, h=h);
    translate([0,0,-clip_avoid])
    cube([r+clip_avoid, r+clip_avoid, h+clip_avoid*2]);
  }
}

module quarter_sphere(r=false){
  intersection(){
    sphere(r=r);
    cube([r+clip_avoid, r+clip_avoid, r+clip_avoid]);
  }
    
}


module slope(l=false, w=false, h=false){
  //first bottom, then top
  points = [[0,0,0],[l,0,0],[l,w,0],[0,w,0],[0,0,h],[0,w,h]];
  faces = [[0,1,2,3],[0,3,5,4],[0,4,1],[2,5,3],[1,4,5,2]];
  polyhedron(points, faces);
}





//the top half of the case
module case_top(){
  
  module base_shape(){
    
    module champfer_cutouts(){
      //space for the champfered edges at:
      //front
      translate([-clip_avoid,-clip_avoid,case_top_height-top_champfer_radius+clip_avoid])
      cube([top_champfer_radius+clip_avoid, case_width+clip_avoid*2, top_champfer_radius+clip_avoid]);
      //back
      translate([case_length-top_champfer_radius,-clip_avoid,case_top_height-top_champfer_radius+clip_avoid])
      cube([top_champfer_radius+clip_avoid, case_width+clip_avoid*2, top_champfer_radius+clip_avoid]);
      //right
      translate([-clip_avoid,-clip_avoid,case_top_height-top_champfer_radius+clip_avoid])
      cube([case_length+clip_avoid*2, top_champfer_radius+clip_avoid, top_champfer_radius+clip_avoid]);
      //left
      translate([-clip_avoid,case_width-top_champfer_radius,case_top_height-top_champfer_radius+clip_avoid])
      cube([case_length+clip_avoid*2, top_champfer_radius+clip_avoid, top_champfer_radius+clip_avoid]);
      translate([0,0,0]){
        //front right
        translate([-clip_avoid,-clip_avoid,-clip_avoid])
        cube([top_champfer_radius+clip_avoid,top_champfer_radius+clip_avoid, case_top_height+top_champfer_radius*2]);
        //front left
        translate([-clip_avoid,case_width-top_champfer_radius,-clip_avoid])
        cube([top_champfer_radius+clip_avoid,top_champfer_radius+clip_avoid, case_top_height+top_champfer_radius*2]);
        //back right
        translate([case_length-top_champfer_radius,-clip_avoid,-clip_avoid])
        cube([top_champfer_radius+clip_avoid,top_champfer_radius+clip_avoid, case_top_height+top_champfer_radius*2]);
        //back left
        translate([case_length-top_champfer_radius,case_width-top_champfer_radius,-clip_avoid])
        cube([top_champfer_radius+clip_avoid,top_champfer_radius+clip_avoid, case_top_height+top_champfer_radius*2]);
      }

    }
      
    module champfer_edges(){
      //champfered edges at:
      translate([0,0,case_top_height-top_champfer_radius]){
        //sides
        //front
        translate([top_champfer_radius,case_width-top_champfer_radius,0])
        rotate([0,0,90])
        rotate([0,-90,0])
        quarter_cylinder(r=top_champfer_radius, h=case_width-top_champfer_radius*2);
        //back
        translate([case_length-top_champfer_radius,top_champfer_radius,0])
        rotate([0,0,-90])
        rotate([0,-90,0])
        quarter_cylinder(r=top_champfer_radius, h=case_width-top_champfer_radius*2);
        //right
        translate([top_champfer_radius,top_champfer_radius,0])
        rotate([0,0,180])
        rotate([0,-90,0])
        quarter_cylinder(r=top_champfer_radius, h=case_length-top_champfer_radius*2);
        //left
        translate([case_length-top_champfer_radius,case_width-top_champfer_radius,0])
        rotate([0,0,0])
        rotate([0,-90,0])
        quarter_cylinder(r=top_champfer_radius, h=case_length-top_champfer_radius*2);
        
              //corners
        //front right
        translate([top_champfer_radius,top_champfer_radius,0])
        rotate([0,0,180])
        quarter_sphere(r=top_champfer_radius);

        translate([top_champfer_radius,case_width-top_champfer_radius,0])
        rotate([0,0,90])
        quarter_sphere(r=top_champfer_radius);

        translate([case_length-top_champfer_radius,top_champfer_radius,0])
        rotate([0,0,-90])
        quarter_sphere(r=top_champfer_radius);

        translate([case_length-top_champfer_radius,case_width-top_champfer_radius,0])
        rotate([0,0,0])
        quarter_sphere(r=top_champfer_radius);
      }
      
      //edges

      //front right  
      translate([top_champfer_radius,top_champfer_radius,0])
      rotate([0,0,180])
      quarter_cylinder(r=top_champfer_radius, h=case_top_height-top_champfer_radius);
        
      //front left
      translate([top_champfer_radius,case_width-top_champfer_radius,0])
      rotate([0,0,90])
      quarter_cylinder(r=top_champfer_radius, h=case_top_height-top_champfer_radius);
  
      //front right  
      translate([case_length-top_champfer_radius,top_champfer_radius,0])
      rotate([0,0,-90])
      quarter_cylinder(r=top_champfer_radius, h=case_top_height-top_champfer_radius);
  
      //front right  
      translate([case_length-top_champfer_radius,case_width-top_champfer_radius,0])
      rotate([0,0,0])
      quarter_cylinder(r=top_champfer_radius, h=case_top_height-top_champfer_radius);
    }
    
    difference(){
      cube([case_length, case_width, case_top_height]);
      champfer_cutouts();
      translate([wall_thickness,wall_thickness,-clip_avoid])
      cube([case_length-wall_thickness*2, case_width-wall_thickness*2, case_top_height-wall_thickness+clip_avoid]);
    }
    champfer_edges();
  }
  
  module button_cutouts(){
    module button_cutout(){
      //recess
      linear_extrude(clad_thickness+clip_avoid)
      button_plate_shape(holes = false);
      //screw and button holes
      translate([0,0,-(wall_thickness + clip_avoid)])
      linear_extrude(wall_thickness + clip_avoid*2)
      difference(){
        translate([clip_avoid, clip_avoid, clip_avoid])
        scale([1 - clip_avoid, 1 - clip_avoid])
        button_plate_shape(holes = false);
        button_plate_shape(holes = true);
      }
      //hole for the pcb
      pcb_hole_length = button_hole_size*4 + button_plate_button_spacing*3;
      translate([button_plate_length/2 - pcb_hole_length/2, button_plate_width/2 - button_hole_size/2, -wall_thickness -clip_avoid])
      cube([pcb_hole_length, button_hole_size, wall_thickness+clip_avoid*2]);
    
    }
    
    //cutouts for right buttons
    translate([button_plate_x_offset, button_plate_border_offset, case_top_height-clad_thickness])
    button_cutout();
    //cutouts for left buttons
    translate([button_plate_x_offset, case_width- button_plate_border_offset - button_plate_width, case_top_height-clad_thickness])
    button_cutout();
  }
  
  module alignment_pins(){
    module alignment_pin(){
      cube([alignment_pin_length/2,alignment_pin_thickness, alignment_pin_height]);
      cube([alignment_pin_thickness,alignment_pin_length/2, alignment_pin_height]);
      translate([0,0,alignment_pin_height]){
        translate([alignment_pin_length/2,0,0])
        slope(h=-alignment_pin_height, w=alignment_pin_thickness, l=alignment_pin_length/2);
        
        translate([alignment_pin_thickness,alignment_pin_length/2,0])
        rotate([0,0,90])
        slope(h=-alignment_pin_height, w=alignment_pin_thickness, l=alignment_pin_length/2);
      }
    }
    
    module cone(){
      radius = sqrt(alignment_pin_length*alignment_pin_length + alignment_pin_thickness*alignment_pin_thickness);
      intersection(){
        cylinder(r1=radius, r2=0, h=case_top_height-wall_thickness);
        cube(radius);
      }
    }
    
    //the pins
    translate([wall_thickness,wall_thickness,0])
    rotate([0,0,0]){
      cone();
      translate([0,0,-alignment_pin_height])
      alignment_pin();
    }
    translate([case_length-wall_thickness,wall_thickness,0])
    rotate([0,0,90]){
      cone();
      translate([0,0,-alignment_pin_height])
      alignment_pin();
    }
    translate([case_length-wall_thickness,case_width-wall_thickness,0])
    rotate([0,0,180]){
      cone();
      translate([0,0,-alignment_pin_height])
      alignment_pin();
    }
    translate([wall_thickness,case_width-wall_thickness,0])
    rotate([0,0,-90]){
      cone();
      translate([0,0,-alignment_pin_height])
      alignment_pin();
    }    
  }
  
  module power_button_cutout(){
    translate([case_length - power_button_offset - power_button_length, case_width/2 - power_button_width/2])
    cube([power_button_length, power_button_width, 100]);
  }

  
  difference(){
    //the piece I begin with
    base_shape();
    button_cutouts();
    power_button_cutout();
    
  }
  //pins for aligning the halves of the case
  alignment_pins();
}


module mainboard(){
    union(){
      //board itself
      linear_extrude(clad_thickness+clip_avoid)
      mainboard_shape();
      //dac
      translate([dac_x_offset, dac_y_offset, dac_z_offset])
      linear_extrude(clad_thickness+clip_avoid)
      dac_shape();
      //nodemcu
      translate([nodeMCU_x_offset, mainboard_width/2-nodeMCU_width/2, nodeMCU_z_offset])
      linear_extrude(nodeMCU_usb_height+clad_thickness+wall_thickness)
      nodeMCU_shape();
    }
}

module case_bottom(){
  
  mainboard_right_front_position = [mainboard_x_offset + wall_thickness, case_width/2 - mainboard_width/2, wall_thickness+clip_avoid+pcb_bottom_layer_spacing];
  bat_board_right_front_position = [
    mainboard_right_front_position[0]+mainboard_length-screw_offset-metric_screw_size/2-spacer_thickness/2 -bat_board_length,
    case_width-bat_board_width-wall_thickness,
    wall_thickness];
  battery_right_front_position = [
    bat_board_right_front_position[0] - wall_thickness - battery_length,
    case_width - wall_thickness - battery_width,
    wall_thickness];
  
  module bat_board(){
    translate(bat_board_right_front_position){
      linear_extrude(bat_board_max_height)
      bat_board_shape();
      
      //usb hole
      translate([bat_board_usb_offset, bat_board_width-clip_avoid,0])
      cube([bat_board_usb_length, wall_thickness+clip_avoid*2,bat_board_usb_height]);
    }
    
  }
  
  module battery(){
    translate(battery_right_front_position)
    linear_extrude(battery_height)
    battery_shape();
  }
  
  module bat_board_holder(){
    translate(bat_board_right_front_position){
      //bar on the dac side of the bat_board
      translate([0,-wall_thickness, 0])
      difference(){
        cube([bat_board_length, wall_thickness, bottom_compontent_holder_height]);
        translate([0,-clip_avoid, 0])
        cube([bat_board_cables_width, wall_thickness+clip_avoid*2, bottom_compontent_holder_height+clip_avoid]);
      }
      //bar on the front side of the bat_board
      translate([-wall_thickness, -wall_thickness, 0])
      cube([wall_thickness, bat_board_width+wall_thickness, bottom_compontent_holder_height]);
    }
  }
  
  module battery_holder(){
    //bar at the front side
    translate([-wall_thickness, -wall_thickness, 0])
    translate(battery_right_front_position)
    cube([wall_thickness, battery_width+wall_thickness, bottom_compontent_holder_height]);
    
    //bar at the right side
    translate(battery_right_front_position)
    translate([0,-wall_thickness,0])
    difference(){
      cube([battery_length, wall_thickness, bottom_compontent_holder_height]);
      translate([battery_length/2 - battery_cables_width/2,-clip_avoid,0])
      cube([battery_cables_width,wall_thickness+clip_avoid*2, bottom_compontent_holder_height+clip_avoid]);
    }
  }
  
  module dac_podest(){
    z_offset = mainboard_right_front_position[2] - wall_thickness + dac_z_offset - audio_jack_diameter;
    podest_width = case_width-wall_thickness-bat_board_width; //clamp in the bat_board with the podest
    
    difference(){
      translate([
        mainboard_right_front_position[0]+mainboard_length-mainboard_neck_length,
        0,
        wall_thickness]){
        //base to rest the dac on
        cube([mainboard_neck_length, podest_width, z_offset]);
        //wall at the back to hold the dac firmly
        cube([(dac_x_offset - (mainboard_length-mainboard_neck_length)) - bad_printer_offset, podest_width, z_offset + audio_jack_diameter + clad_thickness + additional_border_height]);
      }
      
      translate([
        mainboard_right_front_position[0]+mainboard_length-audio_jack_length,
        mainboard_right_front_position[1]+mainboard_width-mainboard_neck_width-audio_jack_offset,
        wall_thickness+z_offset-clad_thickness])
      cube([audio_jack_length+clip_avoid, audio_jack_diameter, clad_thickness+clip_avoid]);
    }
    
  }
  
  module base_shape(){
  
    difference(){
      cube([case_length, case_width, case_bottom_height]);
      //hollow interior
      translate([wall_thickness, wall_thickness, wall_thickness])
      cube([case_length - wall_thickness*2, case_width - wall_thickness*2, case_bottom_height - wall_thickness + clip_avoid]);
      
      //remove cutouts for champfer at:
      //bottom front
      translate([-clip_avoid,-clip_avoid,-clip_avoid])
      cube([bottom_champfer_radius+clip_avoid, case_width+clip_avoid*2, bottom_champfer_radius+clip_avoid]);
      //bottom back
      translate([case_length-bottom_champfer_radius,-clip_avoid,-clip_avoid])
      cube([bottom_champfer_radius+clip_avoid, case_width+clip_avoid*2, bottom_champfer_radius+clip_avoid]);
      //bottom right
      translate([-clip_avoid, -clip_avoid, -clip_avoid])
      cube([case_length+clip_avoid*2,bottom_champfer_radius+clip_avoid, bottom_champfer_radius+clip_avoid]);
      //bottom left
      translate([-clip_avoid, case_width - bottom_champfer_radius, -clip_avoid])
      cube([case_length+clip_avoid*2,bottom_champfer_radius+clip_avoid, bottom_champfer_radius+clip_avoid]);
      //side front right
      translate([-clip_avoid, -clip_avoid, -clip_avoid])
      cube([bottom_champfer_radius+clip_avoid,bottom_champfer_radius+clip_avoid, case_bottom_height+clip_avoid*2]);
      //side front left
      translate([-clip_avoid, case_width-bottom_champfer_radius, -clip_avoid])
      cube([bottom_champfer_radius+clip_avoid,bottom_champfer_radius+clip_avoid, case_bottom_height+clip_avoid*2]);
      //side back right
      translate([case_length-bottom_champfer_radius, -clip_avoid, -clip_avoid])
      cube([bottom_champfer_radius+clip_avoid,bottom_champfer_radius+clip_avoid, case_bottom_height+clip_avoid*2]);
      //side front left
      translate([case_length-bottom_champfer_radius, case_width-bottom_champfer_radius, -clip_avoid])
      cube([bottom_champfer_radius+clip_avoid,bottom_champfer_radius+clip_avoid, case_bottom_height+clip_avoid*2]);
    }
    
    //champfers at the bottom
    //champfer front
    translate([bottom_champfer_radius,bottom_champfer_radius,bottom_champfer_radius])
    rotate([-90,0,0])
    rotate([0,0,90])
    quarter_cylinder(h=case_width-bottom_champfer_radius*2, r=bottom_champfer_radius);
    //champfer back
    translate([case_length-bottom_champfer_radius,bottom_champfer_radius,bottom_champfer_radius])
    rotate([-90,0,0])
    rotate([0,0,0])
    quarter_cylinder(h=case_width-bottom_champfer_radius*2, r=bottom_champfer_radius);
    //champfer right
    translate([bottom_champfer_radius,bottom_champfer_radius,bottom_champfer_radius])
    rotate([0,90,0])
    rotate([0,0,-90])
    quarter_cylinder(h=case_length-bottom_champfer_radius*2, r=bottom_champfer_radius);
    //champfer left
    translate([bottom_champfer_radius,case_width-bottom_champfer_radius,bottom_champfer_radius])
    rotate([0,90,0])
    rotate([0,0,0])
    quarter_cylinder(h=case_length-bottom_champfer_radius*2, r=bottom_champfer_radius);
    //champfers at the sides
    //champfer front right
    translate([bottom_champfer_radius,bottom_champfer_radius,bottom_champfer_radius])
    rotate([0,0,180])
    quarter_cylinder(h=case_bottom_height-bottom_champfer_radius, r=bottom_champfer_radius);
    //champfer front left
    translate([bottom_champfer_radius,case_width-bottom_champfer_radius,bottom_champfer_radius])
    rotate([0,0,90])
    quarter_cylinder(h=case_bottom_height-bottom_champfer_radius, r=bottom_champfer_radius);
    //champfer back right
    translate([case_length-bottom_champfer_radius,bottom_champfer_radius,bottom_champfer_radius])
    rotate([0,0,-90])
    quarter_cylinder(h=case_bottom_height-bottom_champfer_radius, r=bottom_champfer_radius);
    //champfer back left
    translate([case_length-bottom_champfer_radius,case_width-bottom_champfer_radius,bottom_champfer_radius])
    rotate([0,0,0])
    quarter_cylinder(h=case_bottom_height-bottom_champfer_radius, r=bottom_champfer_radius);
    //champfers at the corners
    //champfer front right corner
    translate([bottom_champfer_radius,bottom_champfer_radius,bottom_champfer_radius])
    rotate([0,180,0])
    rotate([0,0,-90])
    quarter_sphere(r=bottom_champfer_radius);
    //champfer front left corner
    translate([bottom_champfer_radius,case_width-bottom_champfer_radius,bottom_champfer_radius])
    rotate([0,180,0])
    rotate([0,0,0])
    quarter_sphere(r=bottom_champfer_radius);
    //champfer back right corner
    translate([case_length-bottom_champfer_radius,bottom_champfer_radius,bottom_champfer_radius])
    rotate([0,180,0])
    rotate([0,0,180])
    quarter_sphere(r=bottom_champfer_radius);
    //champfer back right corner
    translate([case_length-bottom_champfer_radius,bottom_champfer_radius,bottom_champfer_radius])
    rotate([0,180,0])
    rotate([0,0,180])
    quarter_sphere(r=bottom_champfer_radius);
    //champfer back left corner
    translate([case_length-bottom_champfer_radius,case_width-bottom_champfer_radius,bottom_champfer_radius])
    rotate([0,180,0])
    rotate([0,0,90])
    quarter_sphere(r=bottom_champfer_radius);
    
  }
  
  module front_cutouts(){
    translate([0,0,button_plate_front_z_offset])
    union(){
      //actual plate
      translate([-clip_avoid, wall_thickness, wall_thickness])
      rotate([0,0,90])
      rotate([90,0,0])
      linear_extrude(clad_thickness+clip_avoid)
      button_plate_front_shape(holes=false);
      
      
      //recess for content
      translate([clip_avoid, wall_thickness+recess_width + button_plate_front_screw_x_offset + metric_screw_size, wall_thickness+recess_width])
      rotate([0,0,90])
      rotate([90,0,0])
      linear_extrude(wall_thickness+clip_avoid)
      resize([button_plate_front_length-button_plate_front_screw_x_offset*2 -metric_screw_size*2 - recess_width*2, button_plate_front_width-recess_width*2])
      button_plate_front_shape(holes=false);
      
      
      //holes for screws
      translate([-clip_avoid/2, wall_thickness, wall_thickness])
      rotate([0,0,90])
      rotate([90,0,0])
      linear_extrude(wall_thickness + clip_avoid)
      difference(){
        translate([button_plate_front_champfer_radius/2,button_plate_front_champfer_radius/2])
        square([button_plate_front_length - button_plate_front_champfer_radius, button_plate_front_width - button_plate_front_champfer_radius]);
        button_plate_front_shape();
      }
      
      //hole for usb cable
      translate([-clip_avoid, case_width/2 - usb_cable_width/2, mainboard_right_front_position[2]+nodeMCU_z_offset-button_plate_front_z_offset])
      cube([wall_thickness+clip_avoid*2, usb_cable_width, nodeMCU_usb_height+clad_thickness+wall_thickness]); //TODO left of here
        
    }
  }
  
  module spacer(pin_hole=false){
    pin_hole_size = spacer_thickness - metric_screw_size;
    difference(){
      cylinder(d=spacer_thickness, h=mainboard_right_front_position[2]-case_bottom_base_thickness, center=false);
      translate([0,0,-clip_avoid])
      cylinder(d=metric_screw_size, h=mainboard_right_front_position[2]-case_bottom_base_thickness+clip_avoid*2, center=false);
      if(pin_hole){
        translate([sin(45)*(spacer_thickness/2),sin(45)*(spacer_thickness/2),mainboard_right_front_position[2]-case_bottom_base_thickness])
        sphere(d=pin_hole_size);
      }
    }
  }
  
  module audio_jack_hole(){
    aj_y_position = case_width/2 - mainboard_width/2 + dac_length - audio_jack_offset - audio_jack_diameter/2;
    translate([case_length-wall_thickness-clip_avoid/2, aj_y_position, mainboard_right_front_position[2] + dac_z_offset - audio_jack_diameter/2])
    rotate([0,90,0]){
      cylinder(h=wall_thickness+clip_avoid, d=audio_jack_diameter, $fn=100);
      translate([0,0,audio_jack_length+clip_avoid])
      cylinder(h=wall_thickness-audio_jack_length, d=audio_plug_diameter, $fn=100);
      translate([-100,-audio_jack_diameter/2,0])
      cube([100, audio_jack_diameter,audio_jack_length]);
    }
  }
  
  module pcb_screw_holes(){
      translate([mainboard_right_front_position[0], mainboard_right_front_position[1],-clip_avoid]){
      translate([screw_offset+metric_screw_size/2,screw_offset+metric_screw_size/2,0]){
        cylinder(d=metric_screw_size+bad_printer_offset, h=case_bottom_base_thickness+clip_avoid*2);
        nutHole(metric_screw_size);
      }
      translate([screw_offset+metric_screw_size/2,mainboard_width-screw_offset-metric_screw_size/2,0]){
        cylinder(d=metric_screw_size+bad_printer_offset, h=case_bottom_base_thickness+clip_avoid*2);
        nutHole(metric_screw_size);
      }
      translate([mainboard_length-screw_offset-metric_screw_size/2,mainboard_width-screw_offset-metric_screw_size/2,0]){
        cylinder(d=metric_screw_size+bad_printer_offset, h=case_bottom_base_thickness+clip_avoid*2);
        nutHole(metric_screw_size);
      }
    }
  }
  
  module pcb_spacers(){
    translate([mainboard_right_front_position[0], mainboard_right_front_position[1],case_bottom_base_thickness]){
      translate([screw_offset+metric_screw_size/2,screw_offset+metric_screw_size/2,0])
      spacer(pin_hole=true);
      translate([screw_offset+metric_screw_size/2,mainboard_width-screw_offset-metric_screw_size/2,0])
      rotate([0,0,-90])
      spacer(pin_hole=true);
      translate([mainboard_length-screw_offset-metric_screw_size/2,mainboard_width-screw_offset-metric_screw_size/2,0])
      spacer();
    }
  }
  
  difference(){
    base_shape();
    
    translate(mainboard_right_front_position)
    #mainboard();         //including dac and nodemcu
    front_cutouts();     //for button plate and usb connector
    #bat_board();
    #battery();
    audio_jack_hole();
    pcb_screw_holes();
  }
  
  dac_podest();
  battery_holder();
  bat_board_holder();
  pcb_spacers();
}



