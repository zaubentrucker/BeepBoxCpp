// variables of global relevance
//------------------------
clad_thickness = 1.66;
screw_diameter = 3;
bad_printer_offset = 0.1;
bad_printer_factor = 1.01;
// -----------------------



bat_board_width = 23.33;
bat_board_length = 33.21;
bat_board_max_height = 4.52;
bat_board_usb_offset = 2.24;
bat_board_usb_length = 11;
bat_board_usb_height = 4.06 + 1.58*2;
bat_board_cables_width = 6.5;
module bat_board_shape(){
  square([bat_board_length, bat_board_width]);
}


battery_length = 30.4;
battery_width = 36;
battery_height = 4.97;
battery_cables_width = 6.55;
module battery_shape(){
  square([battery_length, battery_width]);
}


mainboard_width = 38;
mainboard_length = 87.5;
mainboard_champfer_radius = 3;
mainboard_neck_length = 21;
mainboard_neck_width = 11;
screw_offset = 1.5;
module mainboard_shape(holes=true){
  
  difference(){
    union(){
      //main part
      translate([mainboard_champfer_radius, mainboard_champfer_radius])
      scale(0.01)
      offset(mainboard_champfer_radius*100)
      scale(100)
      square([mainboard_length-mainboard_champfer_radius*2, mainboard_width-mainboard_champfer_radius*2]);
      
      //dac
      translate([dac_x_offset, dac_y_offset]);
      square([dac_width, dac_length]);
    }
    
    //holes for the screws
    if(holes){
      translate([screw_offset+screw_diameter/2,screw_offset+screw_diameter/2])
      scale(0.01) circle(d=100*screw_diameter);
      translate([mainboard_length - (screw_offset+screw_diameter/2),screw_offset+screw_diameter/2])
      scale(0.01) circle(d=100*screw_diameter);
      translate([screw_offset+screw_diameter/2,mainboard_width - (screw_offset+screw_diameter/2)])
      scale(0.01) circle(d=100*screw_diameter);
      translate([mainboard_length - (screw_offset+screw_diameter/2),mainboard_width - (screw_offset+screw_diameter/2)])
      scale(0.01) circle(d=100*screw_diameter);
    }
    
    //free space below DAC
    translate([mainboard_length - mainboard_neck_length, 0])
    square([mainboard_neck_length, mainboard_width-mainboard_neck_width]);
  }
}


dac_width = 17.1;
dac_length = 32;
dac_x_offset = 70.47;
dac_y_offset = -0.79;
dac_z_offset =  11 + clad_thickness;
module dac_shape(){
  square([dac_width, dac_length]);
}


button_plate_length = 91.44 + bad_printer_offset*2;
button_plate_width = 19.05 + bad_printer_offset*2;
button_plate_champfer_radius = 1.27;

button_plate_button_spacing = 5.2;
button_hole_size = 14; 

button_plate_screw_distance = [82.18, 8.03];
echo(button_plate_screw_distance);
module button_plate_shape(holes=true){  
  difference(){
    //main part
    translate([button_plate_champfer_radius, button_plate_champfer_radius])
    scale(0.01)
    offset(button_plate_champfer_radius*100)
    scale(100)
    square([button_plate_length-button_plate_champfer_radius*2, button_plate_width-button_plate_champfer_radius*2]);
    
    //holes for buttons
    if(holes){
      translate([button_plate_length/2 - 2*button_hole_size - 1.5*button_plate_button_spacing, 0])
      for(i=[0:1:3]){
        translate([(button_hole_size+button_plate_button_spacing)*i, button_plate_width/2 - button_hole_size/2])
        square(button_hole_size);
      }
    }
    
    //holes for the screws
    if(holes){
      translate([button_plate_length/2 - button_plate_screw_distance[0]/2,button_plate_width/2 - button_plate_screw_distance[1]/2]){
        translate([0,0])
        circle(d=screw_diameter);
        translate([button_plate_screw_distance[0],0])
        circle(d=screw_diameter);
        translate([0,button_plate_screw_distance[1]])
        circle(d=screw_diameter);
        translate([button_plate_screw_distance[0],button_plate_screw_distance[1]])
        circle(d=screw_diameter);
      }
    }
  }
}



button_plate_front_length = 52 + bad_printer_offset * 2;
button_plate_front_width = 19 + bad_printer_offset * 2;
button_plate_front_champfer_radius = 1.5;
button_plate_front_button_spacing = 5;
button_plate_front_screw_spacing = 8;
button_plate_front_screw_distance = [44, 11];
button_plate_front_screw_x_offset = 2.5 + screw_diameter/2;
button_plate_front_screw_y_offset = 2.5 + screw_diameter/2;
echo(button_plate_front_screw_distance);

module button_plate_front_shape(holes=true){
 difference(){
    //main part
    translate([button_plate_front_champfer_radius, button_plate_front_champfer_radius])
    scale(0.01)
    offset(button_plate_front_champfer_radius*100)
    scale(100)
    square([button_plate_front_length-button_plate_front_champfer_radius*2, button_plate_front_width-button_plate_front_champfer_radius*2]);
    
    //holes for buttons
    if(holes){
      translate([button_plate_front_length/2 + button_plate_front_button_spacing/2, button_plate_front_width/2 - button_hole_size/2])
      square(button_hole_size);
      translate([button_plate_front_length/2 - button_plate_front_button_spacing/2 - button_hole_size, button_plate_front_width/2 - button_hole_size/2])
      square(button_hole_size);
    }
    
    //holes for the screws
    if(holes){
      translate([button_plate_front_length/2 - button_plate_front_screw_distance[0]/2,button_plate_front_width/2 - button_plate_front_screw_distance[1]/2]){
        translate([0,0])
        circle(d=screw_diameter);
        translate([button_plate_front_screw_distance[0],0])
        circle(d=screw_diameter);
        translate([0,button_plate_front_screw_distance[1]])
        circle(d=screw_diameter);
        translate([button_plate_front_screw_distance[0],button_plate_front_screw_distance[1]])
        circle(d=screw_diameter);
      }
    }
  }
}


nodeMCU_z_offset = 9.6 + clad_thickness;
nodeMCU_x_offset = -4.54;
nodeMCU_length = 51.7;
nodeMCU_width = 28.2;
nodeMCU_max_thickness = 4.84;
nodeMCU_usb_width = 8;
nodeMCU_usb_height = 2.81;
nodeMCU_usb_plug_length = 1.5;
module nodeMCU_shape(){
    square([nodeMCU_length, nodeMCU_width]);
}
